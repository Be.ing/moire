//! Synchronous callbacks
//!
//! Could be attached to UI controls.

use std::{
    future::Future,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

use aoide_core::track::tag::{FACET_COMMENT, FACET_GENRE, FACET_GROUPING};
use aoide_core_api::{
    filtering::StringPredicate,
    track::search::{PhraseFieldFilter, StringField},
};
use aoide_desktop_app::{collection, fs::choose_directory};
use aoide_storage_sqlite::connection::pool::gatekeeper::Gatekeeper;

use moire_library_backend::Backend;

use crate::{Frontend, NESTED_MUSIC_DIRS};

// Simplified, local state management. A more sophisticated solution
// that enables/disables the corresponding buttons accordingly would
// require an application-wide global state management.
static ON_CHOOSE_MUSIC_DIR_GUARD: AtomicBool = AtomicBool::new(false);

pub fn on_music_dir_choose(frontend: &Frontend) -> impl FnMut() {
    let settings_state = Arc::clone(frontend.settings_state());
    move || {
        if ON_CHOOSE_MUSIC_DIR_GUARD
            .compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed)
            .unwrap_or(true)
        {
            // Prevent opening multiple file dialogs at the same time
            log::debug!("Already choosing a music directory");
            return;
        }
        log::info!("Choosing music directory...");
        let settings_state = Arc::clone(&settings_state);
        tokio::spawn(async move {
            let old_music_dir = settings_state.read().music_dir.clone();
            let new_music_dir = choose_directory(old_music_dir.as_deref()).await;
            if let Some(music_dir) = &new_music_dir {
                log::info!("Updating music directory: {}", music_dir.display());
                settings_state.update_music_dir(music_dir);
            }
            ON_CHOOSE_MUSIC_DIR_GUARD
                .compare_exchange(true, false, Ordering::Release, Ordering::Relaxed)
                .expect("infallible");
        });
    }
}

pub fn on_music_dir_reset(frontend: &Frontend) -> impl FnMut() {
    let settings_state = Arc::clone(frontend.settings_state());
    move || {
        log::info!("Resetting music directory");
        settings_state.reset_music_dir();
    }
}

pub fn on_collection_rescan_music_dir(frontend: &Frontend, backend: &Backend) -> impl FnMut() {
    let collection_state = Arc::clone(frontend.collection_state());
    let track_search_state = Arc::clone(frontend.track_search_state());
    let db_gatekeeper = Arc::clone(backend.environment().db_gatekeeper());
    move || {
        let collection_state = Arc::clone(&collection_state);
        let track_search_state = Arc::clone(&track_search_state);
        let db_gatekeeper = Arc::clone(&db_gatekeeper);
        let report_progress_fn =
            |progress: Option<aoide_backend_embedded::batch::rescan_collection_vfs::Progress>| {
                // TODO: The reporting probably needs to be debounced, otherwise it
                // could overload the UI. The synchronous invocation will also slow
                // down the batch task if it takes too much time.
                if let Some(progress) = progress {
                    log::warn!("TODO Report progress while rescanning collection: {progress:?}");
                } else {
                    log::warn!("TODO Report rescanning collection finished");
                }
            };
        let task =
            collection_rescan_music_dir_task(collection_state, db_gatekeeper, report_progress_fn);
        tokio::spawn(async move {
            match task.await {
                Ok(outcome) => {
                    log::warn!("TODO Report outcome after rescanning collection: {outcome:?}");
                    // Discard any cached search results
                    track_search_state.reset_fetched();
                }
                Err(err) => {
                    log::warn!("TODO Report error after rescanning collection: {err}");
                }
            }
        });
    }
}

fn collection_rescan_music_dir_task(
    collection_state: Arc<collection::ObservableState>,
    db_gatekeeper: Arc<Gatekeeper>,
    mut report_progress_fn: impl FnMut(Option<aoide_backend_embedded::batch::rescan_collection_vfs::Progress>)
        + Clone
        + Send
        + 'static,
) -> impl Future<
    Output = anyhow::Result<aoide_backend_embedded::batch::rescan_collection_vfs::Outcome>,
> + Send
       + 'static {
    let mut collection_uid = None;
    collection_state.modify(|state| {
        collection_uid = state.entity_uid().map(ToOwned::to_owned);
        collection_uid.is_some() && state.reset_to_pending()
    });
    async move {
        let collection_uid = if let Some(collection_uid) = collection_uid {
            collection_uid
        } else {
            anyhow::bail!("No collection");
        };
        log::debug!("Rescanning collection...");
        let res = {
            let mut report_progress_fn = report_progress_fn.clone();
            let report_progress_fn = move |progress| {
                report_progress_fn(Some(progress));
            };
            collection::rescan_vfs(&*db_gatekeeper, collection_uid, report_progress_fn).await
        };
        report_progress_fn(None);
        log::debug!("Rescanning collection finished: {:?}", res);
        if let Err(err) = collection_state
            .refresh_from_db(&db_gatekeeper, NESTED_MUSIC_DIRS)
            .await
        {
            log::warn!("Failed to refresh collection after rescanning music directory: {err}");
        }
        res
    }
}

pub fn on_track_search_submit_input(frontend: &Frontend) -> impl FnMut(String) -> String {
    let track_search_state = Arc::clone(frontend.track_search_state());
    move |input| {
        log::debug!("Received search input: {input}");
        let input = input.trim().to_owned();
        let filter = parse_track_search_filter_from_input(&input);
        let mut params = aoide_core_api::track::search::Params {
            filter,
            ..Default::default()
        };
        // Argument is consumed when updating succeeds
        if !track_search_state.update_params(&mut params) {
            log::debug!("Track search params not updated: {params:?}");
        }
        input
    }
}

fn parse_track_search_filter_from_input(
    input: &str,
) -> Option<aoide_core_api::track::search::Filter> {
    debug_assert_eq!(input, input.trim());
    if input.is_empty() {
        return None;
    }
    let fields = [
        StringField::TrackTitle,
        StringField::TrackArtist,
        StringField::AlbumTitle,
        StringField::AlbumArtist,
    ];
    let tag_facets = [
        FACET_COMMENT.to_owned(),
        FACET_GROUPING.to_owned(),
        FACET_GENRE.to_owned(),
    ];
    let all_filters: Vec<_> = input
        .split_whitespace()
        .map(|term| {
            let phrase = aoide_core_api::track::search::Filter::Phrase(PhraseFieldFilter {
                fields: fields.to_vec(),
                terms: vec![term.to_owned()],
            });
            let tag =
                aoide_core_api::track::search::Filter::Tag(aoide_core_api::tag::search::Filter {
                    facets: Some(tag_facets.to_vec()),
                    label: Some(StringPredicate::Contains(term.to_owned())),
                    ..Default::default()
                });
            aoide_core_api::track::search::Filter::Any(vec![phrase, tag])
        })
        .collect();
    debug_assert!(!all_filters.is_empty());
    Some(aoide_core_api::track::search::Filter::All(all_filters))
}
