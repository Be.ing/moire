use std::sync::mpsc;

use basedrop::Owned;

use crate::{io, Clip, Deck};

#[cfg(target_arch = "x86")]
use std::arch::x86::{_MM_FLUSH_ZERO_ON, _MM_SET_FLUSH_ZERO_MODE};
#[cfg(target_arch = "x86_64")]
use std::arch::x86_64::{_MM_FLUSH_ZERO_ON, _MM_SET_FLUSH_ZERO_MODE};

#[allow(clippy::large_enum_variant)]
pub enum Command {
    AddDeck {
        deck: Deck,
        vec: Owned<Vec<Deck>>,
    },
    LoadClip {
        deck_index: usize,
        clip: Clip,
        vec: Owned<Vec<Clip>>,
    },
    SetVolume {
        deck_index: usize,
        value: f32,
    },
    SetTempo {
        deck_index: usize,
        value: f32,
    },
    SetHeadphones {
        deck_index: usize,
        value: bool,
    },
    MoveClip {
        deck_index: usize,
        clip_index: usize,
        diff_seconds: f32,
    },
}

pub enum Event {
    TimeElapsed(f32),
    BufferSizeChanged(usize),
    SampleRateChanged(usize),
}

/// Engine is the central entry point into the audio processing code.
/// When the audio backend API (currently only JACK) requests new buffers
/// of audio to pass to the audio interface, Engine is responsible for
/// telling everything that generates audio to do so, then mixing the signals
/// into buffers that get passed to the audio backend (currently only
/// JackProcessHandler).
pub struct Engine {
    pub buffer_main_out: Vec<Vec<f32>>,
    pub buffer_headphones_out: Vec<Vec<f32>>,
    pub decks: Option<Owned<Vec<Deck>>>,

    sample_rate: usize,

    command_rx: rtrb::Consumer<Command>,
    event_tx: rtrb::Producer<Event>,

    io_event_rx: Option<mpsc::Receiver<io::Event>>,

    playhead_main_frames: f32,
    playhead_headphones_frames: f32,
}

impl Engine {
    pub fn new(command_rx: rtrb::Consumer<Command>, event_tx: rtrb::Producer<Event>) -> Engine {
        Engine {
            buffer_main_out: vec![Vec::<f32>::new(); 2],
            buffer_headphones_out: vec![Vec::<f32>::new(); 2],
            decks: None,
            sample_rate: 0,
            io_event_rx: None,
            event_tx,
            command_rx,
            playhead_main_frames: 0f32,
            playhead_headphones_frames: 0f32,
        }
    }

    pub fn process(&mut self) {
        assert_no_alloc::assert_no_alloc(|| {
            #[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
            unsafe {
                _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
            }

            if let Some(rx) = &self.io_event_rx {
                if let Ok(message) = rx.try_recv() {
                    match message {
                        io::Event::SampleRateChanged(sample_rate) => {
                            self.sample_rate = sample_rate;
                            if let Some(decks) = &mut self.decks {
                                for deck in decks.iter_mut() {
                                    deck.set_sample_rate(sample_rate);
                                }
                            }
                            self.event_tx
                                .push(Event::SampleRateChanged(sample_rate))
                                .unwrap();
                        }
                    }
                }
            }

            while self.command_rx.slots() > 0 {
                match self.command_rx.pop().unwrap() {
                    Command::AddDeck { deck, mut vec } => {
                        if let Some(decks) = &mut self.decks {
                            for _ in 0..decks.len() {
                                vec.push(decks.pop().unwrap());
                            }
                        }
                        vec.push(deck);
                        self.decks = Some(vec);
                    }
                    Command::LoadClip {
                        deck_index,
                        clip,
                        vec,
                    } => {
                        if let Some(decks) = &mut self.decks {
                            let deck = decks
                                .get_mut(deck_index)
                                .expect("Tried to load a clip to a deck that does not exist.");
                            deck.load_clip(clip, vec);
                        }
                    }
                    Command::SetVolume { deck_index, value } => {
                        if let Some(decks) = &mut self.decks {
                            let deck = decks
                                .get_mut(deck_index)
                                .expect("Tried to set volume of a deck that does not exist.");
                            deck.volume.value = value;
                        }
                    }
                    Command::SetTempo { deck_index, value } => {
                        if let Some(decks) = &mut self.decks {
                            let deck = decks
                                .get_mut(deck_index)
                                .expect("Tried to set volume of a deck that does not exist.");
                            deck.set_tempo_ratio(value);
                        }
                    }
                    Command::SetHeadphones { deck_index, value } => {
                        if let Some(decks) = &mut self.decks {
                            let deck = decks.get_mut(deck_index).expect(
                                "Tried to toggle headphones on a deck that does not exist.",
                            );
                            deck.headphones_enabled.value = value as usize as f32;
                        }
                    }
                    Command::MoveClip {
                        deck_index,
                        clip_index,
                        diff_seconds,
                    } => {
                        if let Some(decks) = &mut self.decks {
                            let deck = decks
                                .get_mut(deck_index)
                                .expect("Tried to move a clip on a deck that does not exist.");
                            deck.seek_clip(clip_index, diff_seconds);
                        }
                    }
                }
            }

            for channel in self.buffer_main_out.iter_mut() {
                channel.fill(0f32);
            }
            for channel in self.buffer_headphones_out.iter_mut() {
                channel.fill(0f32);
            }

            if let Some(decks) = &mut self.decks {
                for deck in decks.iter_mut() {
                    deck.process(self.playhead_main_frames, self.playhead_headphones_frames);
                    for (deck_channel, output_channel) in deck
                        .buffer_main_out
                        .iter()
                        .zip(self.buffer_main_out.iter_mut())
                    {
                        for (i, sample) in deck_channel.iter().enumerate() {
                            output_channel[i] += sample;
                        }
                    }
                    for (deck_channel, output_channel) in deck
                        .buffer_headphones_out
                        .iter()
                        .zip(self.buffer_headphones_out.iter_mut())
                    {
                        for (i, sample) in deck_channel.iter().enumerate() {
                            output_channel[i] += sample;
                        }
                    }
                }
            }

            self.playhead_main_frames += self.buffer_main_out[0].len() as f32;
            self.playhead_headphones_frames += self.buffer_headphones_out[0].len() as f32;
            let _ = self.event_tx.push(Event::TimeElapsed(
                self.playhead_main_frames / self.sample_rate as f32,
            ));
        })
    }

    pub fn set_buffer_size(&mut self, buffer_size_frames: usize) {
        for channel in &mut self.buffer_main_out {
            channel.resize(buffer_size_frames, 0f32);
        }
        for channel in &mut self.buffer_headphones_out {
            channel.resize(buffer_size_frames, 0f32);
        }

        // If there are any AddDeck messages waiting in the channel, they need
        // to be added to self.decks here or they will miss the set_buffer_size call below.
        // This can happen on startup if a buffer size change callback is triggered after
        // the GUI creates the initial deck but before audio processing starts.
        // Refer to https://codeberg.org/Be.ing/moire/issues/3
        //
        // There was a bug in Pipewire where this happened even if `node.lock-quantum = true`
        // was configured: https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/2293
        // That bug has been fixed in Pipewire since 0.3.51: https://gitlab.freedesktop.org/pipewire/pipewire/-/commit/2268d1838bf9c1e949d4ed38059d5d709b03e5ec
        // However, that is a valid implementation of the JACK API, even if it is not what
        // Pipewire should have done, so this case should be handled.
        while let Ok(Command::AddDeck { deck: _, vec: _ }) = self.command_rx.peek() {
            if let Command::AddDeck { deck, mut vec } = self.command_rx.pop().unwrap() {
                if let Some(decks) = &mut self.decks {
                    for _ in 0..decks.len() {
                        vec.push(decks.pop().unwrap());
                    }
                }
                vec.push(deck);
                self.decks = Some(vec);
            } else {
                unreachable!("Command was not an AddDeck message?!");
            }
        }
        if let Some(decks) = &mut self.decks {
            for deck in decks.iter_mut() {
                deck.set_buffer_size(buffer_size_frames);
            }
        }

        self.event_tx
            .push(Event::BufferSizeChanged(buffer_size_frames))
            .unwrap();
    }

    pub fn set_io_event_rx(&mut self, rx: mpsc::Receiver<io::Event>) {
        self.io_event_rx = Some(rx);
    }
}
