use aoide_backend_embedded::media::source as api;
use aoide_core::{collection::EntityUid as CollectionUid, media::content::ContentPath};

use crate::{Backend, Result};

pub async fn purge_orphaned(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_core_api::media::source::purge_orphaned::Params,
) -> Result<aoide_core_api::media::source::purge_orphaned::Outcome> {
    api::purge_orphaned(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
    )
    .await
}

pub async fn purge_untracked(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_core_api::media::source::purge_untracked::Params,
) -> Result<aoide_core_api::media::source::purge_untracked::Outcome> {
    api::purge_untracked(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
    )
    .await
}

pub async fn relocate(
    backend: &Backend,
    collection_uid: CollectionUid,
    old_path_prefix: ContentPath,
    new_path_prefix: ContentPath,
) -> Result<usize> {
    api::relocate(
        backend.environment().db_gatekeeper(),
        collection_uid,
        old_path_prefix,
        new_path_prefix,
    )
    .await
}
