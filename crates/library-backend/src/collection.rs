use aoide_backend_embedded::collection as api;

use aoide_core::collection::{Collection, Entity, EntityHeader, EntityUid};

use aoide_core_api::{
    collection::{EntityWithSummary, LoadScope},
    Pagination,
};
use aoide_repo::collection::MediaSourceRootUrlFilter;

use crate::{Backend, Result};

pub async fn load_all_kinds(backend: &Backend) -> Result<Vec<String>> {
    api::load_all_kinds(backend.environment().db_gatekeeper()).await
}

pub async fn load_all(
    backend: &Backend,
    kind: Option<String>,
    media_source_root_url: Option<MediaSourceRootUrlFilter>,
    load_scope: LoadScope,
    pagination: Option<Pagination>,
) -> Result<Vec<EntityWithSummary>> {
    api::load_all(
        backend.environment().db_gatekeeper(),
        kind,
        media_source_root_url,
        load_scope,
        pagination,
    )
    .await
}

pub async fn create(backend: &Backend, new_collection: Collection) -> Result<Entity> {
    api::create(backend.environment().db_gatekeeper(), new_collection).await
}

pub async fn update(
    backend: &Backend,
    entity_header: EntityHeader,
    modified_collection: Collection,
) -> Result<Entity> {
    api::update(
        backend.environment().db_gatekeeper(),
        entity_header,
        modified_collection,
    )
    .await
}

pub async fn purge(backend: &Backend, entity_uid: EntityUid) -> Result<()> {
    api::purge(backend.environment().db_gatekeeper(), entity_uid).await
}
