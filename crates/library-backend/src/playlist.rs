use aoide_backend_embedded::playlist as api;

use aoide_core::{
    collection::EntityUid as CollectionUid,
    playlist::{Entity, EntityHeader, EntityUid, EntityWithEntries, Playlist},
};
use aoide_core_api::playlist::EntityWithEntriesSummary;

use crate::{Backend, Pagination, Result};

pub async fn load_one(backend: &Backend, entity_uid: EntityUid) -> Result<EntityWithEntries> {
    api::load_one(backend.environment().db_gatekeeper(), entity_uid).await
}

pub async fn load_all(
    backend: &Backend,
    collection_uid: CollectionUid,
    kind: Option<String>,
    pagination: Option<Pagination>,
) -> Result<Vec<EntityWithEntriesSummary>> {
    api::load_all(
        backend.environment().db_gatekeeper(),
        collection_uid,
        kind,
        pagination,
    )
    .await
}

pub async fn create(
    backend: &Backend,
    collection_uid: CollectionUid,
    new_playlist: Playlist,
) -> Result<Entity> {
    api::create(
        backend.environment().db_gatekeeper(),
        collection_uid,
        new_playlist,
    )
    .await
}

pub async fn update(
    backend: &Backend,
    entity_header: EntityHeader,
    modified_playlist: Playlist,
) -> Result<Entity> {
    api::update(
        backend.environment().db_gatekeeper(),
        entity_header,
        modified_playlist,
    )
    .await
}

pub async fn purge(backend: &Backend, entity_uid: EntityUid) -> Result<()> {
    api::purge(backend.environment().db_gatekeeper(), entity_uid).await
}

pub async fn patch_entries(
    backend: &Backend,
    entity_header: EntityHeader,
    operations: impl IntoIterator<Item = aoide_usecases_sqlite::playlist::entries::PatchOperation>
        + Send
        + 'static,
) -> Result<EntityWithEntriesSummary> {
    api::entries::patch(
        backend.environment().db_gatekeeper(),
        entity_header,
        operations,
    )
    .await
}
