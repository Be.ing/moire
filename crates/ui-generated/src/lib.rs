#![allow(missing_debug_implementations)]
#![allow(unreachable_pub)]
#![allow(elided_lifetimes_in_paths)]
#![allow(clippy::cmp_owned)]
#![allow(clippy::redundant_clone)]
#![allow(clippy::must_use_candidate)]

slint::include_modules!();
