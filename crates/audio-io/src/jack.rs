use std::sync::mpsc;

use basedrop::{Handle, Owned};
use jack::{
    AsyncClient, AudioOut, ClientOptions, ClientStatus, Control, Frames, NotificationHandler, Port,
    ProcessHandler, ProcessScope,
};
use rtrb::{Consumer, Producer, RingBuffer};

use log::{error, info, warn};

use moire_audio_dsp::{io, Engine as AudioEngine};

pub use jack::Client;

pub enum Command {
    AddDeck {
        port_l: Port<AudioOut>,
        port_r: Port<AudioOut>,
        vec_l: Owned<Vec<Port<AudioOut>>>,
        vec_r: Owned<Vec<Port<AudioOut>>>,
    },
}

/// JackProcessHandler responds to JACK's requests to generate new buffers
/// of audio to pass to the audio interface (or another JACK client).
struct JackProcessHandler {
    audio_engine: AudioEngine,
    out_main_l: Port<AudioOut>,
    out_main_r: Port<AudioOut>,
    out_headphones_l: Port<AudioOut>,
    out_headphones_r: Port<AudioOut>,
    out_decks_l: Owned<Vec<Port<AudioOut>>>,
    out_decks_r: Owned<Vec<Port<AudioOut>>>,
    deck_change_rx: Consumer<Command>,
}

impl JackProcessHandler {
    fn new(
        audio_engine: AudioEngine,
        out_main_l: Port<AudioOut>,
        out_main_r: Port<AudioOut>,
        out_headphones_l: Port<AudioOut>,
        out_headphones_r: Port<AudioOut>,
        garbage_collector: Handle,
    ) -> (JackProcessHandler, Producer<Command>) {
        let (tx, rx) = RingBuffer::<Command>::new(16);
        (
            JackProcessHandler {
                audio_engine,
                out_main_l,
                out_main_r,
                out_headphones_l,
                out_headphones_r,
                out_decks_l: Owned::new(&garbage_collector, Vec::new()),
                out_decks_r: Owned::new(&garbage_collector, Vec::new()),
                deck_change_rx: rx,
            },
            tx,
        )
    }
}

impl ProcessHandler for JackProcessHandler {
    fn process(&mut self, _client: &Client, ps: &ProcessScope) -> Control {
        assert_no_alloc::assert_no_alloc(|| {
            while self.deck_change_rx.slots() > 0 {
                match self.deck_change_rx.pop().unwrap() {
                    Command::AddDeck {
                        port_l,
                        port_r,
                        mut vec_l,
                        mut vec_r,
                    } => {
                        for _ in 0..self.out_decks_l.len() {
                            vec_l.push(self.out_decks_l.pop().unwrap());
                        }
                        vec_l.push(port_l);
                        self.out_decks_l = vec_l;

                        for _ in 0..self.out_decks_r.len() {
                            vec_r.push(self.out_decks_r.pop().unwrap());
                        }
                        vec_r.push(port_r);
                        self.out_decks_r = vec_r;
                    }
                }
            }

            self.audio_engine.process();

            self.out_main_l
                .as_mut_slice(ps)
                .copy_from_slice(&self.audio_engine.buffer_main_out[0]);
            self.out_main_r
                .as_mut_slice(ps)
                .copy_from_slice(&self.audio_engine.buffer_main_out[1]);

            self.out_headphones_l
                .as_mut_slice(ps)
                .copy_from_slice(&self.audio_engine.buffer_headphones_out[0]);
            self.out_headphones_r
                .as_mut_slice(ps)
                .copy_from_slice(&self.audio_engine.buffer_headphones_out[1]);

            if let Some(decks) = &self.audio_engine.decks {
                for (index, deck) in (*decks).iter().enumerate() {
                    self.out_decks_l[index]
                        .as_mut_slice(ps)
                        .copy_from_slice(&deck.buffer_main_out[0]);
                    self.out_decks_r[index]
                        .as_mut_slice(ps)
                        .copy_from_slice(&deck.buffer_main_out[1]);
                }
            }
        });
        Control::Continue
    }

    fn buffer_size(&mut self, _: &Client, buffer_size_frames: Frames) -> Control {
        info!("buffer size changed to {buffer_size_frames} frames");
        self.audio_engine
            .set_buffer_size(buffer_size_frames as usize);
        Control::Continue
    }
}

/// EventAdapter receives notifications from JACK and sends them over
/// a channel to the appropriate targets that needs to respond to them.
struct EventAdapter {
    tx: mpsc::SyncSender<io::Event>,
}

impl EventAdapter {
    pub fn new(tx: mpsc::SyncSender<io::Event>) -> EventAdapter {
        EventAdapter { tx }
    }
}

impl NotificationHandler for EventAdapter {
    fn shutdown(&mut self, status: ClientStatus, reason: &str) {
        info!("shutdown with status {status:?} because \"{reason}\"");
    }

    fn freewheel(&mut self, _: &Client, is_enabled: bool) {
        info!(
            "freewheel mode {}",
            if is_enabled { "enabled" } else { "disabled" }
        );
    }

    fn sample_rate(&mut self, _: &Client, sample_rate: Frames) -> Control {
        info!("sample rate changed to {sample_rate}");
        let _ = self
            .tx
            .send(io::Event::SampleRateChanged(sample_rate as usize));
        Control::Continue
    }

    fn xrun(&mut self, _: &Client) -> Control {
        info!("xrun occurred");
        Control::Continue
    }
}

/// JackBackend initializes a new JACK client, creates and connects its ports,
/// and tells JACK to start processing.
pub struct JackBackend {
    async_client: AsyncClient<EventAdapter, JackProcessHandler>,
}

impl JackBackend {
    pub fn client(&self) -> &Client {
        self.async_client.as_client()
    }
}

fn check_jack_connection(result: Result<(), jack::Error>, port_number: usize) {
    if result.is_err() {
        error!("Error connecting to JACK output system:playback_{port_number}: {result:?}");
    }
}

impl JackBackend {
    pub fn new(
        garbage_collector: Handle,
        mut audio_engine: AudioEngine,
        client_name: &str,
    ) -> (JackBackend, Producer<Command>) {
        let (jack_client, _status) =
            Client::new(client_name, ClientOptions::NO_START_SERVER).unwrap();

        let out_main_l = jack_client
            .register_port("main_L", AudioOut::default())
            .unwrap();
        let out_main_r = jack_client
            .register_port("main_R", AudioOut::default())
            .unwrap();
        let out_headphones_l = jack_client
            .register_port("headphones_L", AudioOut::default())
            .unwrap();
        let out_headphones_r = jack_client
            .register_port("headphones_R", AudioOut::default())
            .unwrap();

        audio_engine.set_buffer_size(jack_client.buffer_size() as usize);

        // This channel doesn't really have multiple producers; the only producer is
        // the JACK notification thread. However, rtrb::RingBuffer doesn't work in this
        // case because Client::activate_async requires the NotificationHandler
        // to implement Sync, but rtrb::Producer does not implement Sync.
        let (io_event_tx, io_event_rx) = mpsc::sync_channel(64);
        audio_engine.set_io_event_rx(io_event_rx);

        let (process_handler, jack_process_tx) = JackProcessHandler::new(
            audio_engine,
            out_main_l,
            out_main_r,
            out_headphones_l,
            out_headphones_r,
            garbage_collector,
        );
        let event_adapter = EventAdapter::new(io_event_tx);
        let async_client = jack_client
            .activate_async(event_adapter, process_handler)
            .unwrap();
        // activate_async consumes the client but it is still needed below.
        let jack_client = async_client.as_client();

        // JACK2 requires that connections are made *after* the client starts processing
        // and Pipewire JACK deadlocks if ports are connected in the process callback, so
        // ports need to be connected here. However JackProcessHandler::new had to take
        // ownership of the ports created above, so create new handles to the ports.
        let out_main_l = jack_client
            .port_by_name(&format!("{}:main_L", client_name))
            .unwrap();
        let out_main_r = jack_client
            .port_by_name(&format!("{}:main_R", client_name))
            .unwrap();
        let out_headphones_l = jack_client
            .port_by_name(&format!("{}:headphones_L", client_name))
            .unwrap();
        let out_headphones_r = jack_client
            .port_by_name(&format!("{}:headphones_R", client_name))
            .unwrap();

        let out_system_1 = jack_client.port_by_name("system:playback_1").unwrap();
        let out_system_2 = jack_client.port_by_name("system:playback_2").unwrap();

        let out_system_3 = jack_client.port_by_name("system:playback_3");
        let out_system_4 = jack_client.port_by_name("system:playback_4");
        if let (Some(out_system_3), Some(out_system_4)) = (out_system_3, out_system_4) {
            check_jack_connection(
                jack_client.connect_ports(&out_headphones_l, &out_system_3),
                3,
            );
            check_jack_connection(
                jack_client.connect_ports(&out_headphones_r, &out_system_4),
                4,
            );
        } else {
            warn!("JACK system outputs 3 & 4 not found; not connecting headphones output ports.");
        }

        check_jack_connection(jack_client.connect_ports(&out_main_l, &out_system_1), 1);
        check_jack_connection(jack_client.connect_ports(&out_main_r, &out_system_2), 2);

        (JackBackend { async_client }, jack_process_tx)
    }
}
