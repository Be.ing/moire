use std::{cell::RefCell, path::PathBuf, rc::Rc};

use basedrop::Owned;
use slint::{ComponentHandle, Model, ModelRc, VecModel, Weak};

use aoide_desktop_app::settings;

use moire::waveform_analyzer::{WaveformAnalysis, WaveformAnalyzer};

use moire_audio_dsp::{engine as audio_engine, Clip as AudioClip, Deck as AudioDeck};
use moire_audio_io::jack::{self, JackBackend};

pub use moire_library_backend::Backend as LibraryBackend;
pub use moire_library_frontend::{callback as library_callback, Frontend as LibraryFrontend};

use moire_ui_generated::{ClipProps, DeckProps, MainWindow};

mod tasklet;

pub struct Gui {
    main_window: MainWindow,
    _library_backend: LibraryBackend,
    _library_frontend: LibraryFrontend,
    _poll_audio_rx_timer: slint::Timer,
    _buffer_size_frames: Rc<RefCell<usize>>,
    _sample_rate: Rc<RefCell<usize>>,
    _to_audio_tx: Rc<RefCell<rtrb::Producer<audio_engine::Command>>>,
    _jack_backend: Rc<JackBackend>,
}

/// Gui is responsible for running the GUI after the rest of the
/// application has been initialized.
impl Gui {
    #[allow(clippy::too_many_arguments)] // FIXME
    pub fn new(
        config_dir: PathBuf,
        library_backend: LibraryBackend,
        initial_settings: settings::State,
        to_audio_tx: rtrb::Producer<audio_engine::Command>,
        mut from_audio_rx: rtrb::Consumer<audio_engine::Event>,
        to_jack_tx: rtrb::Producer<jack::Command>,
        jack_backend: Rc<JackBackend>,
        gc_handle: basedrop::Handle,
    ) -> Gui {
        let buffer_size_frames = Rc::new(RefCell::new(0));
        let sample_rate = Rc::new(RefCell::new(0));
        let to_audio_tx = Rc::new(RefCell::new(to_audio_tx));

        let main_window = MainWindow::new();

        let main_window_weak = main_window.as_weak();

        main_window.on_volume_changed_on_deck(on_volume_changed_on_deck(&to_audio_tx));
        main_window.on_tempo_changed_on_deck(on_tempo_changed_on_deck(&to_audio_tx));

        // The buffer size must be known before creating decks.
        // The initial BufferSizeChanged message comes from querying JACk before activating
        // the JACK client. Then, sometimes Pipewire calls the JACK buffer sized changed
        // callback before it starts calling the JACK process callback. In that case, there
        // will be two BufferSizeChanged callbacks in the channel, so this needs to loop.
        while let Ok(audio_engine::Event::BufferSizeChanged(_)) = from_audio_rx.peek() {
            if let Ok(audio_engine::Event::BufferSizeChanged(frames)) = from_audio_rx.pop() {
                let mut buffer_size_frames = buffer_size_frames.borrow_mut();
                *buffer_size_frames = frames;
            } else {
                unreachable!("audio_engine::Event not a BufferSizeChanged message?!")
            }
        }
        let mut add_deck_callback = on_add_deck(
            &main_window_weak,
            &buffer_size_frames,
            &to_audio_tx,
            to_jack_tx,
            &jack_backend,
            &gc_handle,
        );
        add_deck_callback();
        main_window.on_add_deck(add_deck_callback);

        main_window.on_load_file_to_deck(on_load_file_to_deck(
            &main_window_weak,
            &buffer_size_frames,
            &sample_rate,
            &to_audio_tx,
            &gc_handle,
        ));

        main_window.on_clip_seek_on_deck(on_clip_seek_on_deck(&to_audio_tx));

        main_window.on_headphones_changed_on_deck(on_headphones_changed_on_deck(&to_audio_tx));

        let poll_audio_rx_timer = slint::Timer::default();
        poll_audio_rx_timer.start(
            slint::TimerMode::Repeated,
            std::time::Duration::from_millis(100),
            on_gui_poll_timer(
                &main_window_weak,
                &buffer_size_frames,
                &sample_rate,
                from_audio_rx,
            ),
        );

        let library_frontend = LibraryFrontend::default();
        library_frontend.spawn_tasklets(&library_backend, config_dir);

        // Connect library backend properties (updated by tasklets) and callbacks

        tasklet::spawn_music_dir_changed_property_updater(main_window.as_weak(), &library_frontend);
        main_window.on_music_dir_choose(library_callback::on_music_dir_choose(&library_frontend));
        main_window.on_music_dir_reset(library_callback::on_music_dir_reset(&library_frontend));

        tasklet::spawn_collection_property_updater(main_window.as_weak(), &library_frontend);
        main_window.on_collection_rescan_music_dir(
            library_callback::on_collection_rescan_music_dir(&library_frontend, &library_backend),
        );

        tasklet::spawn_track_search_property_updater(main_window.as_weak(), &library_frontend);
        main_window.on_track_search_submit_input({
            let mut on_track_search_submit_input =
                library_callback::on_track_search_submit_input(&library_frontend);
            move |input| on_track_search_submit_input(input.into()).into()
        });

        // Propagate the initial settings through the connected FRP network
        // after wiring everything together and before showing the UI.
        // This will also save the aoide settings persistently to disk.
        library_frontend.settings_state().modify(move |settings| {
            *settings = initial_settings;
            true
        });

        Gui {
            main_window,
            _library_backend: library_backend,
            _library_frontend: library_frontend,
            _poll_audio_rx_timer: poll_audio_rx_timer,
            _buffer_size_frames: buffer_size_frames,
            _sample_rate: sample_rate,
            _to_audio_tx: to_audio_tx,
            _jack_backend: jack_backend,
        }
    }

    pub fn run(&self) {
        self.main_window.run();
    }
}

fn on_volume_changed_on_deck(
    to_audio_tx: &Rc<RefCell<rtrb::Producer<audio_engine::Command>>>,
) -> impl FnMut(i32, f32) {
    let to_audio_tx = Rc::downgrade(to_audio_tx);
    move |deck_index, value| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(audio_engine::Command::SetVolume {
                deck_index: deck_index as usize,
                value,
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_tempo_changed_on_deck(
    to_audio_tx: &Rc<RefCell<rtrb::Producer<audio_engine::Command>>>,
) -> impl FnMut(i32, f32) {
    let to_audio_tx = Rc::downgrade(to_audio_tx);
    move |deck_index, value| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(audio_engine::Command::SetTempo {
                deck_index: deck_index as usize,
                value,
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_add_deck(
    main_window: &Weak<MainWindow>,
    buffer_size_frames: &Rc<RefCell<usize>>,
    to_audio_tx: &Rc<RefCell<rtrb::Producer<audio_engine::Command>>>,
    mut to_jack_tx: rtrb::Producer<jack::Command>,
    jack_backend: &Rc<JackBackend>,
    gc_handle: &basedrop::Handle,
) -> impl FnMut() {
    let main_window = main_window.clone();
    let buffer_size_frames = Rc::downgrade(buffer_size_frames);
    let to_audio_tx = Rc::downgrade(to_audio_tx);
    let gc_handle = gc_handle.clone();
    let jack_backend = Rc::downgrade(jack_backend);
    move || {
        let main_window = main_window.upgrade().unwrap();
        let decks = main_window.get_decks();
        let deck_count = match decks.as_any().downcast_ref::<VecModel<DeckProps>>() {
            None => {
                let model = VecModel::from(vec![empty_deckdata()]);
                main_window.set_decks(ModelRc::new(model));
                1
            }
            Some(decks) => {
                decks.push(empty_deckdata());
                decks.row_count()
            }
        };

        let deck_index = deck_count;

        let jack_backend = jack_backend.upgrade().unwrap();
        let jack_client = jack_backend.client();
        let port_l = jack_client
            .register_port(&format!("deck{deck_index}_L"), Default::default())
            .unwrap();
        let port_r = jack_client
            .register_port(&format!("deck{deck_index}_R"), Default::default())
            .unwrap();
        let vec_l = Owned::new(&gc_handle, Vec::with_capacity(deck_count));
        let vec_r = Owned::new(&gc_handle, Vec::with_capacity(deck_count));
        to_jack_tx
            .push(jack::Command::AddDeck {
                port_l,
                port_r,
                vec_l,
                vec_r,
            })
            .unwrap();

        let mut new_deck = AudioDeck::default();
        let buffer_size_frames = *buffer_size_frames.upgrade().unwrap().borrow();
        new_deck.set_buffer_size(buffer_size_frames);

        let vec = Vec::<AudioDeck>::with_capacity(deck_count);

        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(audio_engine::Command::AddDeck {
                deck: new_deck,
                vec: Owned::new(&gc_handle, vec),
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_load_file_to_deck(
    main_window: &Weak<MainWindow>,
    buffer_size_frames: &Rc<RefCell<usize>>,
    sample_rate: &Rc<RefCell<usize>>,
    to_audio_tx: &Rc<RefCell<rtrb::Producer<audio_engine::Command>>>,
    gc_handle: &basedrop::Handle,
) -> impl FnMut(i32, f32) {
    let main_window = main_window.clone();
    let to_audio_tx = Rc::downgrade(to_audio_tx);
    let buffer_size_frames = Rc::downgrade(buffer_size_frames);
    let sample_rate = Rc::downgrade(sample_rate);
    let gc_handle = gc_handle.clone();
    move |index, time| {
        let dialog_opened = std::time::Instant::now();

        // TODO: make async
        let path = match rfd::FileDialog::new().pick_file() {
            Some(picked) => picked,
            None => return,
        };

        let start_on_timeline_seconds = time + dialog_opened.elapsed().as_secs_f32();

        let main_window = main_window.upgrade().unwrap();
        let decks = main_window.get_decks();
        let decks = decks
            .as_any()
            .downcast_ref::<VecModel<DeckProps>>()
            .unwrap();
        let deck = decks.row_data(index as usize).unwrap_or_default();

        let mut waveform_analyzer = WaveformAnalyzer::new(&path);
        waveform_analyzer.analyze();
        let waveform = draw_waveform(waveform_analyzer.result());

        let clips = deck
            .clips
            .as_any()
            .downcast_ref::<VecModel<ClipProps>>()
            .unwrap();
        clips.push(ClipProps {
            start_on_timeline_seconds,
            waveform,
        });

        let vec = Vec::with_capacity(clips.row_count());

        let buffer_size_frames = buffer_size_frames.upgrade().unwrap();
        let buffer_size_frames = buffer_size_frames.borrow();
        let sample_rate = sample_rate.upgrade().unwrap();
        let sample_rate = sample_rate.borrow();

        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(audio_engine::Command::LoadClip {
                deck_index: index as usize,
                clip: AudioClip::new(
                    *sample_rate,
                    *buffer_size_frames,
                    &path,
                    start_on_timeline_seconds,
                    0.0,
                    None,
                ),
                vec: Owned::new(&gc_handle, vec),
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_clip_seek_on_deck(
    to_audio_tx: &Rc<RefCell<rtrb::Producer<audio_engine::Command>>>,
) -> impl FnMut(i32, i32, f32) {
    let to_audio_tx = Rc::downgrade(to_audio_tx);
    move |deck_index, clip_index, diff_seconds| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(audio_engine::Command::MoveClip {
                deck_index: deck_index as usize,
                clip_index: clip_index as usize,
                diff_seconds,
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_headphones_changed_on_deck(
    to_audio_tx: &Rc<RefCell<rtrb::Producer<audio_engine::Command>>>,
) -> impl FnMut(i32, bool) {
    let to_audio_tx = Rc::downgrade(to_audio_tx);
    move |index, value| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(audio_engine::Command::SetHeadphones {
                deck_index: index as usize,
                value,
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_gui_poll_timer(
    main_window: &Weak<MainWindow>,
    buffer_size_frames: &Rc<RefCell<usize>>,
    sample_rate: &Rc<RefCell<usize>>,
    mut from_audio_rx: rtrb::Consumer<audio_engine::Event>,
) -> impl FnMut() {
    let main_window = main_window.clone();
    let buffer_size_frames = Rc::downgrade(buffer_size_frames);
    let sample_rate = Rc::downgrade(sample_rate);
    move || {
        let mut last_received_time = 0f32;
        while from_audio_rx.slots() > 0 {
            match from_audio_rx.pop().unwrap() {
                audio_engine::Event::TimeElapsed(t) => last_received_time = t,
                audio_engine::Event::BufferSizeChanged(frames) => {
                    let buffer_size_frames = buffer_size_frames.upgrade().unwrap();
                    let mut buffer_size_frames = buffer_size_frames.borrow_mut();
                    *buffer_size_frames = frames;
                }
                audio_engine::Event::SampleRateChanged(new_sample_rate) => {
                    let sample_rate = sample_rate.upgrade().unwrap();
                    let mut sample_rate = sample_rate.borrow_mut();
                    *sample_rate = new_sample_rate;
                }
            }
        }
        let main_window = main_window.upgrade().unwrap();
        main_window.set_time(last_received_time);
        if last_received_time != 0f32 {
            let hours = (last_received_time / 3600f32).floor();
            let minutes = (last_received_time / 60f32).floor();
            let seconds = (last_received_time % 60f32).floor();
            let deciseconds =
                ((last_received_time - last_received_time.floor() as f32) * 10f32).floor();
            let string = format!("{}:{}:{}.{}", hours, minutes, seconds, deciseconds);
            main_window.set_duration_text(string.into());
        }
    }
}

fn empty_deckdata() -> DeckProps {
    DeckProps {
        clips: ModelRc::new(VecModel::from(vec![])),
    }
}

fn draw_waveform(analysis_data: &WaveformAnalysis) -> slint::Image {
    let height = 200;
    let mut waveform_buffer =
        slint::SharedPixelBuffer::<slint::Rgba8Pixel>::new(analysis_data.len() as u32, height);
    let mut pixmap = tiny_skia::PixmapMut::from_bytes(
        waveform_buffer.make_mut_bytes(),
        analysis_data.len() as u32,
        height as u32,
    )
    .unwrap();
    pixmap.fill(tiny_skia::Color::WHITE);

    let path = {
        let mut path_builder = tiny_skia::PathBuilder::new();
        for (x, bin) in analysis_data.iter().enumerate() {
            path_builder.line_to(
                x as f32,
                height as f32 / 2.0 + bin.minimum * height as f32 / 2.0,
            );
            path_builder.line_to(
                x as f32,
                height as f32 / 2.0 + bin.maximum * height as f32 / 2.0,
            );
        }
        path_builder.finish().unwrap()
    };

    let mut paint = tiny_skia::Paint::default();
    paint.set_color_rgba8(0, 0, 255, 255);
    paint.anti_alias = true;

    pixmap.stroke_path(
        &path,
        &paint,
        &Default::default(),
        Default::default(),
        Default::default(),
    );

    slint::Image::from_rgba8_premultiplied(waveform_buffer)
}
