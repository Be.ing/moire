use aoide_backend_embedded::media::tracker as api;
use aoide_core::collection::EntityUid as CollectionUid;
use aoide_media::io::import::ImportTrackConfig;

use crate::{Backend, Result};

pub async fn query_status(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_core_api::media::tracker::query_status::Params,
) -> Result<aoide_core_api::media::tracker::Status> {
    api::query_status(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
    )
    .await
}

pub async fn scan_directories<P>(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_core_api::media::tracker::scan_directories::Params,
    report_progress_fn: P,
) -> Result<aoide_core_api::media::tracker::scan_directories::Outcome>
where
    P: FnMut(aoide_usecases::media::tracker::scan_directories::ProgressEvent) + Send + 'static,
{
    api::scan_directories(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
        report_progress_fn,
    )
    .await
}

pub async fn untrack_directories(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_core_api::media::tracker::untrack_directories::Params,
) -> Result<aoide_core_api::media::tracker::untrack_directories::Outcome> {
    api::untrack_directories(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
    )
    .await
}

pub async fn import_files<P>(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_core_api::media::tracker::import_files::Params,
    import_config: ImportTrackConfig,
    report_progress_fn: P,
) -> Result<aoide_core_api::media::tracker::import_files::Outcome>
where
    P: FnMut(aoide_usecases::media::tracker::import_files::ProgressEvent) + Send + 'static,
{
    api::import_files(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
        import_config,
        report_progress_fn,
    )
    .await
}

pub async fn find_untracked_files<P>(
    backend: &Backend,
    collection_uid: CollectionUid,
    params: aoide_core_api::media::tracker::find_untracked_files::Params,
    report_progress_fn: P,
) -> Result<aoide_core_api::media::tracker::find_untracked_files::Outcome>
where
    P: FnMut(aoide_usecases::media::tracker::find_untracked_files::ProgressEvent) + Send + 'static,
{
    api::find_untracked_files(
        backend.environment().db_gatekeeper(),
        collection_uid,
        params,
        report_progress_fn,
    )
    .await
}
