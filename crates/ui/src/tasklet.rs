use std::{future::Future, sync::Arc};

use discro::tasklet::OnChanged;
use slint::Weak;

use aoide_desktop_app::{collection, fs::DirPath, settings, track};

use moire_ui_generated::MainWindow;

fn music_dir_update_properties_in_event_loop(
    ui_handle: &Weak<MainWindow>,
    music_dir: Option<&DirPath<'_>>,
) {
    log::debug!("Updating music directory properties: {music_dir:?}");
    let music_dir_path = music_dir
        .map(|dir_path| dir_path.display().to_string())
        .unwrap_or_default();
    ui_handle.upgrade_in_event_loop({
        move |ui| {
            ui.set_music_dir_is_valid(!music_dir_path.is_empty());
            ui.set_music_dir_path(music_dir_path.into());
        }
    });
}

fn on_music_dir_changed_property_updater(
    ui_handle: Weak<MainWindow>,
    library_frontend: &crate::LibraryFrontend,
) -> impl Future<Output = ()> + Send + 'static {
    let subscriber = library_frontend.settings_state().subscribe();
    settings::tasklet::on_music_dir_changed(subscriber, move |music_dir| {
        music_dir_update_properties_in_event_loop(&ui_handle, music_dir);
        OnChanged::Continue
    })
}

pub(super) fn spawn_music_dir_changed_property_updater(
    ui_handle: Weak<MainWindow>,
    library_frontend: &crate::LibraryFrontend,
) {
    tokio::spawn(on_music_dir_changed_property_updater(
        ui_handle,
        library_frontend,
    ));
}

fn u64_to_i32_saturating(value: u64) -> i32 {
    value.min(i32::MAX as u64) as i32
}

fn collection_update_properties_in_event_loop(
    ui_handle: &Weak<MainWindow>,
    state: &collection::State,
) {
    log::debug!("Updating collection properties: {state:?}");
    let (is_idle, is_ready, entity_uid, tracks_count, playlists_count) = match state {
        collection::State::Ready(entity_with_summary) => {
            let entity_uid = entity_with_summary.entity.hdr.uid.to_string();
            let (tracks_count, playlists_count) =
                if let Some(summary) = &entity_with_summary.summary {
                    (
                        u64_to_i32_saturating(summary.tracks.total_count),
                        u64_to_i32_saturating(summary.playlists.total_count),
                    )
                } else {
                    (0, 0)
                };
            (
                state.is_idle(),
                true,
                entity_uid,
                tracks_count,
                playlists_count,
            )
        }
        _ => (
            state.is_idle(),
            false,
            state
                .entity_uid()
                .map(ToString::to_string)
                .unwrap_or_default(),
            0,
            0,
        ),
    };
    ui_handle.upgrade_in_event_loop({
        move |ui| {
            ui.set_collection_is_idle(is_idle);
            ui.set_collection_is_ready(is_ready);
            ui.set_collection_uid(entity_uid.into());
            ui.set_collection_tracks_count(tracks_count);
            ui.set_collection_playlists_count(playlists_count);
        }
    });
}

fn on_collection_property_updater(
    ui_handle: Weak<MainWindow>,
    library_frontend: &crate::LibraryFrontend,
) -> impl Future<Output = ()> + Send + 'static {
    let observable_state = Arc::clone(library_frontend.collection_state());
    let subscriber = observable_state.subscribe();
    collection::tasklet::on_state_tag_changed(subscriber, move |_| {
        collection_update_properties_in_event_loop(&ui_handle, &*observable_state.read());
        OnChanged::Continue
    })
}

pub(super) fn spawn_collection_property_updater(
    ui_handle: Weak<MainWindow>,
    library_frontend: &crate::LibraryFrontend,
) {
    tokio::spawn(on_collection_property_updater(ui_handle, library_frontend));
}

fn track_search_update_properties_in_event_loop(
    ui_handle: &Weak<MainWindow>,
    state: &track::repo_search::State,
) {
    log::debug!("Updating track search properties");
    let is_enabled = state.context().collection_uid.is_some();
    let is_idle = state.is_idle();
    ui_handle.upgrade_in_event_loop({
        move |ui| {
            ui.set_track_search_is_enabled(is_enabled);
            ui.set_track_search_is_idle(is_idle);
        }
    });
}

fn on_track_search_property_updater(
    ui_handle: Weak<MainWindow>,
    library_frontend: &crate::LibraryFrontend,
) -> impl Future<Output = ()> + Send + 'static {
    let observable_state = Arc::clone(library_frontend.track_search_state());
    let subscriber = observable_state.subscribe();
    track::repo_search::tasklet::on_fetch_state_tag_changed(subscriber, move |_| {
        track_search_update_properties_in_event_loop(&ui_handle, &*observable_state.read());
        OnChanged::Continue
    })
}

pub(super) fn spawn_track_search_property_updater(
    ui_handle: Weak<MainWindow>,
    library_frontend: &crate::LibraryFrontend,
) {
    tokio::spawn(on_track_search_property_updater(
        ui_handle,
        library_frontend,
    ));
}
