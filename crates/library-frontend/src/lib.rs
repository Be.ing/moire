use std::{path::PathBuf, sync::Arc};

use aoide_desktop_app::{collection, settings, track};

use moire_library_backend::Backend;

pub mod callback;

const NESTED_MUSIC_DIRS: collection::NestedMusicDirectoriesStrategy =
    collection::NestedMusicDirectoriesStrategy::Permit;

const TRACK_REPO_SEARCH_PREFETCH_LIMIT: usize = 100;

/// Stateful library frontend.
///
/// Manages the application state that should not depend on any
/// particular UI technology.
#[derive(Default)]
pub struct Frontend {
    settings_state: Arc<settings::ObservableState>,
    collection_state: Arc<collection::ObservableState>,
    track_search_state: Arc<track::repo_search::ObservableState>,
}

impl Frontend {
    /// Spawn the frontend observables with its tasklets.
    ///
    /// Only call this once!
    pub fn spawn_tasklets(&self, backend: &Backend, settings_dir: PathBuf) {
        let Self {
            settings_state,
            collection_state,
            track_search_state,
        } = self;
        tokio::spawn(settings::tasklet::on_state_changed_save_to_file(
            settings_state.subscribe(),
            settings_dir,
            |err| {
                log::error!("Failed to save settings to file: {err}");
            },
        ));
        tokio::spawn(collection::tasklet::on_settings_changed(
            Arc::clone(backend.environment().db_gatekeeper()),
            Arc::clone(settings_state),
            Arc::clone(collection_state),
            NESTED_MUSIC_DIRS,
            |err| {
                log::error!("Failed to update collection after music directory changed: {err}");
            },
        ));
        tokio::spawn({
            let collection_state = Arc::clone(collection_state);
            let track_search_state = Arc::clone(track_search_state);
            track::repo_search::tasklet::on_collection_changed(collection_state, track_search_state)
        });
        tokio::spawn({
            let db_gatekeeper = Arc::clone(backend.environment().db_gatekeeper());
            let track_search_state = Arc::clone(track_search_state);
            track::repo_search::tasklet::on_should_prefetch(
                db_gatekeeper,
                track_search_state,
                Some(TRACK_REPO_SEARCH_PREFETCH_LIMIT),
            )
        });
    }

    /// Observable settings state.
    pub fn settings_state(&self) -> &Arc<settings::ObservableState> {
        &self.settings_state
    }

    /// Observable collection state.
    pub fn collection_state(&self) -> &Arc<collection::ObservableState> {
        &self.collection_state
    }

    /// Observable track (repo) search state.
    pub fn track_search_state(&self) -> &Arc<track::repo_search::ObservableState> {
        &self.track_search_state
    }
}
